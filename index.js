const Bus = require('./bus');

function main(){
    let bis = new Bus('Aka', 'DK 1234 BR', 'Biru', 10000, 40);

    console.log('Selamat datang di bus', bis.nama,' dengan plat nomor', bis.plat);
    console.log('Bus ini berwarna', bis.warna, ' dengan Maksimal Penumpang berjumlah', bis.maxPenumpang);
    console.log('Tarif perkilometer dari Bus ini ialah', bis.tarif);
    
    bis.drive(40);
    bis.stop('Sinombong');
    bis.rem(20);
}

main();