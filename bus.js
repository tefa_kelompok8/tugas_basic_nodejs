class Bus {
    constructor(nama, plat, warna, tarif, maxPenumpang){
        this.nama = nama;
        this.plat = plat;
        this.warna = warna;
        this.tarif = tarif;
        this.maxPenumpang = maxPenumpang;
    }

    drive(kecepatan){
        console.log('Kecepatan Bus saat ini', kecepatan,'Km/Jam');
    }

    stop(halte){
        console.log('Pemberhentian selanjutnya di halte', halte);
    }

    rem(penguranganKecepatan){
        console.log('Bus perlahan mengurangi kecepatan sebanyak',penguranganKecepatan,'Km/Jam');
    }
}

module.exports = Bus;